let smoothie = require('smoothie');

let chart = new smoothie.SmoothieChart({responsive: true, interpolation: 'linear', limitFPS: 30, minValue: 0, maxValue: 4096});
let series1 = new smoothie.TimeSeries();
let series2 = new smoothie.TimeSeries();
chart.addTimeSeries(series1, {lineWidth:2,strokeStyle:'#00ff00'});
chart.addTimeSeries(series2, {lineWidth:2,strokeStyle:'#ff0000'});
chart.streamTo(document.getElementById("chart"));

let url = new URL(window.location);
url.protocol = "ws";
url.pathname = "/pressure";

if(url.hostname == "localhost") { 
	url.hostname = "192.168.43.213";
	url.port = 80;
}

let socket = new WebSocket(url);
socket.binaryType = "arraybuffer";
socket.onerror = function (ev) {
	console.log("Socket error", ev);
};


let timebase = 0;
socket.onmessage = function(ev) {
	const packetLen = 8 + 4 + 4;
	
	for(let i=0; i < ev.data.byteLength; i += packetLen) {
		
		let view = new DataView(ev.data, i, packetLen)
		let time = view.getFloat64(0, true);
		let s1 = view.getInt32(8, true);
		let s2 = view.getInt32(8+4, true);
		if(timebase == 0 && i == 0) {
			timebase = Date.now() - time*1000;
		}
		
		time *= 1000;
		time += timebase;
		series1.append(time, s1);
		series2.append(time, s2);
	}
};
