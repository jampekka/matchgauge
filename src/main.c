#include "mgos.h"
#include "mgos_adc.h"
#include "mgos_http_server.h"
#include <driver/adc.h>

int sensor1_pin = 34;
int sensor2_pin = 35;
int nSessions = 0;

const double REPORT_RATE = 0.1;
const int BUFFER_SIZE = 4096;
const double SAMPLING_RATE = 1000.0;

struct sample {
	double time;
	int sensor1;
	int sensor2;
};

struct session {
	int at;
	struct sample* buffer;
	mgos_timer_id timer;
};

void write_buffered(void *arg) {
  	LOG(LL_DEBUG, ("Writing data"));
	struct mg_connection *c = (struct mg_connection*)arg;
	struct session *session = c->user_data;
  	//LOG(LL_INFO, ("%d\t%d", v1, v2));
	if(session->at < 0) return;
	//struct sample sample = session->buffer[session->at];
	/*mg_printf_websocket_frame(c, WEBSOCKET_OP_BINARY, "%d\t%d\n", sample.sensor1, sample.sensor2);*/
	mg_send_websocket_frame(c, WEBSOCKET_OP_BINARY, session->buffer, sizeof(*(session->buffer))*session->at);
	session->at = -1;
  	LOG(LL_DEBUG, ("Data written"));
}

void write_sample(void *arg) {
	struct mg_connection *c = (struct mg_connection*)arg;
	double time = mg_time();
	int v1 = mgos_adc_read(sensor1_pin);
	int v2 = mgos_adc_read(sensor2_pin);
	mg_printf_websocket_frame(c, WEBSOCKET_OP_BINARY, "%f%d\t%d\n", time, v1, v2);

}
void read_sample(void *arg) {
	struct session *session = (struct session*)arg;
	if(session->at >= BUFFER_SIZE) {
		return;
	}
	
	double time = mg_time();
	int v1 = mgos_adc_read(sensor1_pin);
	int v2 = mgos_adc_read(sensor2_pin);
	
	struct sample* sample = session->buffer + ++session->at;
	sample->time = time;
	sample->sensor1 = v1;
	sample->sensor2 = v2;
}

void send_data(struct mg_connection *c, int ev, void *p, void *user) {
  	//LOG(LL_INFO, ("Event %d", ev));
	//if(ev == MG_EV_SEND) {
	//	write_sample(c);
	//}
	//
	
	if(ev == MG_EV_WEBSOCKET_HANDSHAKE_DONE) {
  		LOG(LL_DEBUG, ("Starting session"));
		struct session *session = c->user_data = malloc(sizeof(struct session));
		session->at = -1;
		session->buffer = malloc(sizeof(struct sample)*BUFFER_SIZE);
		session->timer = mgos_set_timer((int)((1.0/SAMPLING_RATE)*1000), MGOS_TIMER_REPEAT, read_sample, session);
		mg_set_timer(c, mg_time() + REPORT_RATE);
  		LOG(LL_DEBUG, ("Session started"));
	}

	if(ev == MG_EV_TIMER) {
		//write_sample(c);
		write_buffered(c);
		mg_set_timer(c, mg_time() + REPORT_RATE);
	}

	if(ev == MG_EV_CLOSE) {
		if(!c->user_data) return;
		struct session *session = (struct session*)c->user_data;
		mgos_clear_timer(session->timer);
		if(session->buffer) free(session->buffer);
		free(session);
	}

	//if(ev != MG_EV_HTTP_REQUEST) return;

	//mg_send_response_line(c, 200, "Content-Type: text/plain\r\n");
	//mg_send_head(c, 200, -1, "Content-Type: text/plain");
	//mg_printf_http_chunk(c, "Jou!\n");
	//mgos_set_timer(1000, MGOS_TIMER_REPEAT, write_sample, NULL);
}

enum mgos_app_init_result mgos_app_init(void) {

	esp32_set_channel_attenuation(sensor1_pin, ADC_ATTEN_DB_11);
	esp32_set_channel_attenuation(sensor2_pin, ADC_ATTEN_DB_11);
	mgos_adc_enable(sensor1_pin);
	mgos_adc_enable(sensor2_pin);

	mgos_register_http_endpoint("/pressure", send_data, NULL);

	//mgos_set_timer(1000, MGOS_TIMER_REPEAT, read_sample, NULL);
	return MGOS_APP_INIT_SUCCESS;
}
